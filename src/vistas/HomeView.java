/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vistas;

import controlador.UsuarioController;

import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Emi
 */
public class HomeView extends javax.swing.JFrame {
    UsuarioController usuarioController=new UsuarioController();
    /**
     * Creates new form HomeView
     */
    public HomeView() {
        initComponents();
        //this.setLocationRelativeTo(null);
        ImageIcon imagen = new ImageIcon("src/vistas/img/pimg.png");
        Icon icono = new ImageIcon(imagen.getImage().getScaledInstance(imagenHomelbl.getWidth(),imagenHomelbl.getHeight(),Image.SCALE_DEFAULT));
        imagenHomelbl.setIcon(icono);
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        usuarioTxt = new javax.swing.JTextField();
        passwordTxf = new javax.swing.JPasswordField();
        IniciarBtn = new javax.swing.JButton();
        SalirBtn = new javax.swing.JButton();
        imagenHomelbl = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        usuarioTxt.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        usuarioTxt.setToolTipText("");

        passwordTxf.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N

        IniciarBtn.setText("Iniciar sesión");
        IniciarBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        IniciarBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                IniciarBtnMouseClicked(evt);
            }
        });
        IniciarBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IniciarBtnActionPerformed(evt);
            }
        });

        SalirBtn.setText("Salir");
        SalirBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        SalirBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                SalirBtnMouseClicked(evt);
            }
        });
        SalirBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SalirBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(99, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(imagenHomelbl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(passwordTxf, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(IniciarBtn, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 224, Short.MAX_VALUE)
                    .addComponent(SalirBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(usuarioTxt, javax.swing.GroupLayout.Alignment.LEADING))
                .addGap(100, 100, 100))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(imagenHomelbl, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 59, Short.MAX_VALUE)
                .addComponent(usuarioTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(passwordTxf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(IniciarBtn)
                .addGap(18, 18, 18)
                .addComponent(SalirBtn)
                .addGap(45, 45, 45))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void SalirBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SalirBtnActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_SalirBtnActionPerformed

    private void SalirBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_SalirBtnMouseClicked
        int op=JOptionPane.showConfirmDialog(null,"seguro quieres salir?");
        if(op==JOptionPane.YES_OPTION){
            dispose();
        }
    }//GEN-LAST:event_SalirBtnMouseClicked

    private void IniciarBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_IniciarBtnMouseClicked
        if(!usuarioTxt.getText().equals("")&& !passwordTxf.getPassword().toString().equals("")){
            String passwordIngresada = new String(passwordTxf.getPassword());
           if(usuarioController.existeUsuario(usuarioTxt.getText(), passwordIngresada)){  
                crearVentanaAdminView(usuarioTxt.getText());
                JOptionPane.showConfirmDialog(null,"Bienvenido "+usuarioTxt.getText(),"Bienvenido",JOptionPane.DEFAULT_OPTION);
                dispose();
           }else{
               JOptionPane.showConfirmDialog(null,"Error en los datos ingresados","Alerta",JOptionPane.DEFAULT_OPTION);
               passwordTxf.setText("");
           }
        }else{
            JOptionPane.showConfirmDialog(null,"Ingresa datos antes de iniciar sección","Alerta",JOptionPane.DEFAULT_OPTION);
        }
        
    }//GEN-LAST:event_IniciarBtnMouseClicked

    public void crearVentanaAdminView(String user){
        AdministradorMarcaView ventana = new AdministradorMarcaView();
        ventana.setSize(724,484);
        ventana.setLocation(500, 100);
        ventana.setVisible(true);
        ventana.setUsernameTxt(user);
        JFrame jf= new JFrame();        //JFrame me permite ejecutar la ventana.
        jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jf.setSize(760, 530);
        jf.setResizable(false);
        jf.setTitle("Administración de concesionaria");
        jf.add(ventana);
        jf.setBounds(1, 1, 760, 530);
        jf.setLocation(0, 0);
        jf.setVisible(true);
        //this.setVisible(false);
    }
    private void IniciarBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IniciarBtnActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_IniciarBtnActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(HomeView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(HomeView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(HomeView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(HomeView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new HomeView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton IniciarBtn;
    private javax.swing.JButton SalirBtn;
    private javax.swing.JLabel imagenHomelbl;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPasswordField passwordTxf;
    private javax.swing.JTextField usuarioTxt;
    // End of variables declaration//GEN-END:variables
}
