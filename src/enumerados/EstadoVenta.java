/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package enumerados;

/**
 *
 * @author Emi
 */
public enum EstadoVenta {
    PENDIENTE,
    ESPERA_DE_PAGO,
    CONFIRMADA,
    PAGADA,
    CANCELADA;
}
