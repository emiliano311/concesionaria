/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helper;

import entidades.Auto;
import entidades.Automovil;
import entidades.Venta;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;


/**
 *
 * @author Emi
 */
public class VentaDetalleHelper {
    @Id
   @Column(name="ventadetalleid", unique=true, nullable=false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int ventadetalleid;
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="auto_id", nullable=false)
    private Auto auto;
     @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="venta_id", nullable=false)
    private Venta venta;

    public VentaDetalleHelper() {
    }

    public VentaDetalleHelper(int ventadetalleid, Auto auto, Venta venta) {
        this.ventadetalleid = ventadetalleid;
        this.auto = auto;
        this.venta = venta;
    }

    public VentaDetalleHelper(Auto auto, Venta venta) {
        this.auto = auto;
        this.venta = venta;
    }

    public int getVentadetalleid() {
        return ventadetalleid;
    }

    public void setVentadetalleid(int ventadetalleid) {
        this.ventadetalleid = ventadetalleid;
    }

    public Auto getAuto() {
        return auto;
    }

    public void setAuto(Auto auto) {
        this.auto = auto;
    }

    public Venta getVenta() {
        return venta;
    }

    public void setVenta(Venta venta) {
        this.venta = venta;
    }

   
    
    
}
