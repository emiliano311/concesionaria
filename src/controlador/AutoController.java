/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import entidades.Auto;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

/**
 *
 * @author Emi
 */
public class AutoController {
    
    public void guardarAuto(Auto auto){
        factoriaSesionesHibernateUtilOLd factoriaUtil = new factoriaSesionesHibernateUtilOLd();
        SessionFactory session = factoriaUtil.getSessionFactory();
        Session sessionAcesso = session.openSession();
        Transaction transaction= sessionAcesso.beginTransaction();
        sessionAcesso.persist(auto);
        transaction.commit();
    }

    
    public List<Auto> listarAutos(){
        factoriaSesionesHibernateUtilOLd factoriautil = new factoriaSesionesHibernateUtilOLd();
        SessionFactory session= factoriautil.getSessionFactory();
        Session sessionAcceso= session.openSession();
        Transaction transaction = sessionAcceso.beginTransaction();
        CriteriaBuilder builder = (CriteriaBuilder) sessionAcceso.getCriteriaBuilder();
        List<Auto> listamodelos=   sessionAcceso.createCriteria(Auto.class).list();
        
        return  listamodelos;
    }
    
    public List<Auto> listarAutosNoVendidos(){
        factoriaSesionesHibernateUtilOLd factoriautil = new factoriaSesionesHibernateUtilOLd();
        SessionFactory session= factoriautil.getSessionFactory();
        Session sessionAcceso= session.openSession();
        Transaction transaction = sessionAcceso.beginTransaction();
        CriteriaBuilder builder = (CriteriaBuilder) sessionAcceso.getCriteriaBuilder();
        List<Auto> listamodelos=   sessionAcceso.createSQLQuery("SELECT a.autoid,a.numerochasis,a.patente,a.automovil_id,a.precioventa,a.color FROM auto as a LEFT JOIN public.\"ventaDetalle\" ON a.autoid=public.\"ventaDetalle\".auto_id where public.\"ventaDetalle\".auto_id is null ;").addEntity(Auto.class).list();
        
        return  listamodelos;
    }
    
    public void modificarAuto(int id ,int numerochasis,String patente,Double precio,String color,int modelo){
        factoriaSesionesHibernateUtilOLd factoriaUtil = new factoriaSesionesHibernateUtilOLd();
        SessionFactory session = factoriaUtil.getSessionFactory();
        Session sessionAcceso = session.openSession();
        Transaction transaction = sessionAcceso.beginTransaction();
        Query query = sessionAcceso.createSQLQuery("UPDATE public.auto SET numerochasis="+numerochasis+", patente='"+patente+"', precioventa="+precio+",color='"+color+"',automovil_id="+modelo+" WHERE autoid ="+id);
        query.executeUpdate();
        transaction.commit();
    }
    
    public Auto getAuto(int id){
        factoriaSesionesHibernateUtilOLd fabricaUtil= new factoriaSesionesHibernateUtilOLd();
        SessionFactory session = fabricaUtil.getSessionFactory();
        Session sessionAcceso= session.openSession();
        Transaction transaction = sessionAcceso.beginTransaction();
        Auto  a=(Auto) sessionAcceso.createSQLQuery("SELECT * from auto WHERE autoid="+id).addEntity(Auto.class).uniqueResult();
        return a;
    }
   
}

