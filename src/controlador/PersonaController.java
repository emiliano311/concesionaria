/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import entidades.Cliente;
import entidades.Persona;
import java.util.Date;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.mapping.Map;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;

public class PersonaController {


public void guardarPersona(Persona persona){
   // Persona persona = new Persona(nombre,apellido,dni,fechanacimiento,telefono,direccion,ciudad,codpostal);
    factoriaSesionesHibernateUtilOLd factoriaUtil = new factoriaSesionesHibernateUtilOLd();
    SessionFactory session = factoriaUtil.getSessionFactory();
    Session sessionAcceso =session.openSession();
    Transaction transaction = sessionAcceso.beginTransaction();
    sessionAcceso.save(persona);
    transaction.commit();
}    

public void guardarCliente(Cliente cliente){
    factoriaSesionesHibernateUtilOLd factoriaUtil = new factoriaSesionesHibernateUtilOLd();
    SessionFactory session = factoriaUtil.getSessionFactory();
    Session sessionAcceso =session.openSession();
    Transaction transaction = sessionAcceso.beginTransaction();
    sessionAcceso.save(cliente);
    transaction.commit();
        
}

public List<Persona> listarPersonas(){
    SessionFactory session =factoriaSesionesHibernateUtilOLd.getSessionFactory();
    Session sessionAcceso= session.openSession();
    Transaction transaction = sessionAcceso.beginTransaction();
    CriteriaBuilder builder =sessionAcceso.getCriteriaBuilder();
    List<Persona> listpersona=sessionAcceso.createCriteria(Persona.class).list();
    return listpersona;
}
public List<Persona> listarPersonasClientes(){
    SessionFactory session = factoriaSesionesHibernateUtilOLd.getSessionFactory();
    Session sessionAcceso= session.openSession();
     
    //CriteriaQuery<Persona> query= (CriteriaQuery<Persona>) sessionAcceso.createSQLQuery("select p.personaid,p.nombre,p.apellido,p.dni,p.fechanacimiento,p.telefono,p.direccion,p.ciudad,p.codpostal from persona p,cliente c where p.personaid=c.persona_id ");
    //Query query=sessionAcceso.createQuery("SELECT p.personaid,p.nombre,p.apellido,p.dni,p.fechanacimiento,p.telefono,p.direccion,p.ciudad,p.codpostal from persona p,cliente INNER JOIN p.personaid cliente.persona_id");
    //Query query=sessionAcceso.createSQLQuery("select p.personaid,p.nombre,p.apellido,p.dni,p.fechanacimiento,p.telefono,p.direccion,p.ciudad,p.codpostal from persona p,cliente c where p.personaid=c.persona_id").addEntity(Persona.class);
    
    List<Persona>listpersona=null;
    listpersona = (List<Persona>) sessionAcceso.createSQLQuery("select p.personaid,p.nombre,p.apellido,p.dni,p.fechanacimiento,p.telefono,p.direccion,p.ciudad,p.codpostal from persona p,cliente c where p.personaid=c.persona_id").addEntity(Persona.class).list();
    
    return listpersona;
    }

public Persona getPersona(int id) {
         Persona persona = new Persona();
         SessionFactory session = factoriaSesionesHibernateUtilOLd.getSessionFactory();
         Session sessionAcceso = session.openSession();
         Transaction transaction = sessionAcceso.beginTransaction();
         CriteriaQuery<Persona> query = (CriteriaQuery<Persona>) sessionAcceso.createSQLQuery("Select * from persona where id= " + id );
        return persona;
        
}

public void modificarPersona(long id,String nombre,String apellido,String dni,String fechanacimiento,String telefono,String direccion,String ciudad,String codpostal,String fechaalta,String email){
    factoriaSesionesHibernateUtilOLd facctoriaSession = new factoriaSesionesHibernateUtilOLd();
    SessionFactory session = factoriaSesionesHibernateUtilOLd.getSessionFactory();
    Session sessionAcceso=session.openSession();
    Transaction transaction =sessionAcceso.beginTransaction();
    Query query = sessionAcceso.createQuery("UPDATE public.persona SET nombre='"+nombre+"', apellido='"+apellido+"', dni='"+dni+"', fechanacimiento='"+fechanacimiento+"', telefono='"+telefono+"', direccion='"+direccion+"', ciudad='"+ciudad+"', codpostal='"+codpostal+"' WHERE personaid="+id);
    query.executeUpdate();
    transaction.commit();
}



    
}
