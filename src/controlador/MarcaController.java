/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import entidades.Marca;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Order;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;


public class MarcaController {
    
    public void guardarMarca(String nombre, String paisDeOrigen ){
            /*Creamos el objeto a persistir*/
        Marca marca = new Marca(nombre, paisDeOrigen);

        // SessionFactory session para conectarnos por medio de Hibernate a la DB
        factoriaSesionesHibernateUtilOLd factoriaUtil = new factoriaSesionesHibernateUtilOLd();
        SessionFactory session = factoriaUtil.getSessionFactory();
        Session sessionAcceso = session.openSession();
        
        
        /* 
            uso beginTransanction() para obtener acceso a standard transaction API <code>UserTransaction</code> y
            begin a transaction on this thread of execution.
        */
        Transaction transaction = sessionAcceso.beginTransaction();
        sessionAcceso.save(marca);
        transaction.commit();
       
        
           
    }
    public void modificarMarca(Long id,String nombre,String paisDeOrigen){
        factoriaSesionesHibernateUtilOLd factoriaUtil = new factoriaSesionesHibernateUtilOLd();
        SessionFactory session = factoriaUtil.getSessionFactory();
        Session sessionAcceso = session.openSession();
        Transaction transaction = sessionAcceso.beginTransaction();
        Query query = sessionAcceso.createSQLQuery("UPDATE public.marca SET nombre='"+nombre+"', paisdeorigen='"+paisDeOrigen+"' WHERE marcaid ="+id);
        query.executeUpdate();
        transaction.commit();
        
        
    }
    
    public void eliminarMarca(Long id ){
        // SessionFactory session para conectarnos por medio de Hibernate a la DB
        factoriaSesionesHibernateUtilOLd factoriaUtil = new factoriaSesionesHibernateUtilOLd();
        SessionFactory session = factoriaUtil.getSessionFactory();
        Session sessionAcceso = session.openSession();
        Transaction transaction = sessionAcceso.beginTransaction();
        Query query = sessionAcceso.createSQLQuery("DELETE  FROM public.marca WHERE marcaid ="+id);
        query.executeUpdate();
        transaction.commit();
        
    }
    
    public List<Marca> listarMarcas(){
        SessionFactory session = factoriaSesionesHibernateUtilOLd.getSessionFactory();
        Session sessionAcceso = session.openSession();
        Transaction transaction = sessionAcceso.beginTransaction();
        CriteriaBuilder builder = sessionAcceso.getCriteriaBuilder();
        List<Marca> listaMarca = sessionAcceso.createCriteria(Marca.class ).list();
        
        return listaMarca;
    }
    
    public Marca getMarca(String nombre){
        factoriaSesionesHibernateUtilOLd fabricaUtil= new factoriaSesionesHibernateUtilOLd();
        SessionFactory session = fabricaUtil.getSessionFactory();
        Session sessionAcceso= session.openSession();
        Transaction transaction = sessionAcceso.beginTransaction();
        
       Marca  m=(Marca) sessionAcceso.createSQLQuery("SELECT * from marca WHERE nombre='"+nombre+"'").addEntity(Marca.class).uniqueResult();
        return m;
    }
    
}
