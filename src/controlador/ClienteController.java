/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import entidades.Cliente;
import entidades.Persona;
import java.util.List;
import javax.persistence.criteria.CriteriaQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

/**
 *
 * @author Emi
 */
public class ClienteController {
    
    
    
 public void guardarPersona(Persona persona){
   // Persona persona = new Persona(nombre,apellido,dni,fechanacimiento,telefono,direccion,ciudad,codpostal);
    factoriaSesionesHibernateUtilOLd factoriaUtil = new factoriaSesionesHibernateUtilOLd();
    SessionFactory session = factoriaUtil.getSessionFactory();
    Session sessionAcceso =session.openSession();
    Transaction transaction = sessionAcceso.beginTransaction();
    sessionAcceso.save(persona);
    transaction.commit();
    
}    

public void guardarCliente(Cliente cliente){
    factoriaSesionesHibernateUtilOLd factoriaUtil = new factoriaSesionesHibernateUtilOLd();
    SessionFactory session = factoriaUtil.getSessionFactory();
    Session sessionAcceso =session.openSession();
    Transaction transaction = sessionAcceso.beginTransaction();
    sessionAcceso.save(cliente);
    transaction.commit();
   
        
}

public List<Persona> listarPersonasClientes(){
    SessionFactory session = factoriaSesionesHibernateUtilOLd.getSessionFactory();
    Session sessionAcceso= session.openSession();
     
    //CriteriaQuery<Persona> query= (CriteriaQuery<Persona>) sessionAcceso.createSQLQuery("select p.personaid,p.nombre,p.apellido,p.dni,p.fechanacimiento,p.telefono,p.direccion,p.ciudad,p.codpostal from persona p,cliente c where p.personaid=c.persona_id ");
    //Query query=sessionAcceso.createQuery("SELECT p.personaid,p.nombre,p.apellido,p.dni,p.fechanacimiento,p.telefono,p.direccion,p.ciudad,p.codpostal from persona p,cliente INNER JOIN p.personaid cliente.persona_id");
    //Query query=sessionAcceso.createSQLQuery("select p.personaid,p.nombre,p.apellido,p.dni,p.fechanacimiento,p.telefono,p.direccion,p.ciudad,p.codpostal from persona p,cliente c where p.personaid=c.persona_id").addEntity(Persona.class);
    
    List<Persona>listpersona=null;
    listpersona = (List<Persona>) sessionAcceso.createSQLQuery("select p.personaid,p.nombre,p.apellido,p.dni,p.fechanacimiento,p.telefono,p.direccion,p.ciudad,p.codpostal from persona p,cliente c where p.personaid=c.persona_id").addEntity(Persona.class).list();
    
    return listpersona;
    
    }

public Cliente getCliente(int id){
    
    SessionFactory session = factoriaSesionesHibernateUtilOLd.getSessionFactory();
    Session sesion = session.openSession();
    Transaction transaction = sesion.beginTransaction();
    Cliente c=null;
    c= (Cliente) sesion.createSQLQuery("select * from cliente WHERE persona_id="+id).addEntity(Cliente.class).uniqueResult();
    
    return c;
}
public Persona getClienteDni(String dni){
    factoriaSesionesHibernateUtilOLd fabricaUtil= new factoriaSesionesHibernateUtilOLd();
        SessionFactory session = fabricaUtil.getSessionFactory();
        Session sessionAcceso= session.openSession();
        Transaction transaction = sessionAcceso.beginTransaction();
        Persona  c=(Persona) sessionAcceso.createSQLQuery("SELECT p.personaid,p.nombre,p.apellido,p.dni,p.fechanacimiento,p.telefono,p.direccion,p.ciudad,p.codpostal FROM persona p, cliente c where p.dni='"+dni+"' AND p.personaid=c.persona_id ").addEntity(Persona.class).uniqueResult();
        return c;
    
}


public void modificarPersona(int id,String nombre,String apellido,String dni,String fechanacimiento,String telefono,String direccion,String ciudad,String codpostal){
   factoriaSesionesHibernateUtilOLd factoriaUtil = new factoriaSesionesHibernateUtilOLd();
   SessionFactory session = factoriaUtil.getSessionFactory();
   Session sessionAcceso= session.openSession();
   Transaction transaction = sessionAcceso.beginTransaction();
   Query query = sessionAcceso.createSQLQuery("UPDATE public.persona SET nombre='"+nombre+"', apellido='"+apellido+"', dni='"+dni+"', fechanacimiento='"+fechanacimiento+"', telefono='"+telefono+"', direccion='"+direccion+"', ciudad='"+ciudad+"', codpostal='"+codpostal+"' WHERE personaid="+id).addEntity(Persona.class);
   query.executeUpdate();
   transaction.commit();
   
}
public void modificarCliente(int id,String fechaalta,String email){
   SessionFactory session = factoriaSesionesHibernateUtilOLd.getSessionFactory();
   Session sessionAcceso= session.openSession();
   Transaction transaction = sessionAcceso.beginTransaction();
   Query query = sessionAcceso.createSQLQuery("UPDATE public.cliente SET fechaalta='"+fechaalta+"',email='"+email+"' WHERE persona_id="+id).addEntity(Cliente.class);
   query.executeUpdate();
   transaction.commit();
  
}
        
}
