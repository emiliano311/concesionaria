/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;
import entidades.Automovil;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author Emi
 */
public class AutoMovilController {
    
    
    public void guardarAutoMovil(Automovil autoMovil){
        factoriaSesionesHibernateUtilOLd factoriaUtil = new factoriaSesionesHibernateUtilOLd();
        SessionFactory session = factoriaUtil.getSessionFactory();
        Session sessionAcesso = session.openSession();
        Transaction transaction= sessionAcesso.beginTransaction();
        sessionAcesso.save(autoMovil);
        transaction.commit();
        
    }
    
    public List<Automovil> listarModelos(){
        factoriaSesionesHibernateUtilOLd factoriautil = new factoriaSesionesHibernateUtilOLd();
        SessionFactory session= factoriautil.getSessionFactory();
        Session sessionAcceso= session.openSession();
        Transaction transaction = sessionAcceso.beginTransaction();
        CriteriaBuilder builder = (CriteriaBuilder) sessionAcceso.getCriteriaBuilder();
        List<Automovil> listamodelos=   sessionAcceso.createCriteria(Automovil.class).list();
        
        return  listamodelos;
    }
    
    public Automovil getAutomovil(String modelo,Long marcaid,int anioFabricacion){
        factoriaSesionesHibernateUtilOLd fabricaUtil= new factoriaSesionesHibernateUtilOLd();
        SessionFactory session = fabricaUtil.getSessionFactory();
        Session sessionAcceso= session.openSession();
        Transaction transaction = sessionAcceso.beginTransaction();
        Automovil  a=(Automovil) sessionAcceso.createSQLQuery("SELECT * from automovil WHERE modelo='"+modelo+"' and marcaid="+marcaid+" and aniofabricacion="+anioFabricacion+"").addEntity(Automovil.class).uniqueResult();
        return a;
    }
    
}
